import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,NgModel } from '@angular/forms';
import {Routes,RouterModule} from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { TodolistModule} from './module/todolist/todolist.module'; 
import {UsermanagementModule} from './module/usermanagement/usermanagement.module'; 
import {routerConfig} from './app.router';
import {AuthenticationModule} from './module/authentication/authentication.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TodolistModule,
    UsermanagementModule,    
    AuthenticationModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routerConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
