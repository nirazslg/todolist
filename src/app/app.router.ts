import {ModuleWithProviders} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';
import {AppComponent} from './app.component';

export const routerConfig: Routes =[
    {
        path: '', 
        redirectTo:'login',
        pathMatch :'full'
    }
    ,
     {
        path: '**', 
        redirectTo:'login',
        pathMatch :'full'
    }
 ];

