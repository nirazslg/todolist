import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserlistComponent } from './userlist/userlist.component';
import { UsercreateComponent } from './usercreate/usercreate.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [UserlistComponent, UsercreateComponent]
})
export class UsermanagementModule { }
