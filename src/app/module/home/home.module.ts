import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomecomponentComponent } from './homecomponent/homecomponent.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HomecomponentComponent]
})
export class HomeModule { }
