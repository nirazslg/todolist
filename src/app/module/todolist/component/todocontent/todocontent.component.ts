import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todocontent',
  templateUrl: './todocontent.component.html',
  styleUrls: ['./todocontent.component.css']
})
export class TodocontentComponent implements OnInit {
  @Output() deleteTask = new EventEmitter();
  constructor() { 
  
  }  
 
  @Input() todolist:any;  
  heroes = ['one','two','three','four','five','six']; 
  ngOnInit() {    
  }    
  onclick(task){
      this.deleteTask.emit(task);
  }
}
