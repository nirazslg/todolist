import { Component, OnInit } from '@angular/core';
import todos from '../../store';

@Component({
  selector: 'app-todohead',
  templateUrl: './todohead.component.html',
  styleUrls: ['./todohead.component.css']
})
export class TodoheadComponent implements OnInit {
  constructor() { }
  ngOnInit() {
  } 
  worktodo:string;
  worklist = todos.todos;
  public onclick(){
    todos.addTodo(this.worktodo);
    this.worktodo ="";    
    console.log(todos);
  }
  handleDelete(deleteTaskObj){   
    debugger;
    todos.removeTodo(deleteTaskObj);
  }
}
