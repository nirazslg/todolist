import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoheadComponent } from './todohead.component';

describe('TodoheadComponent', () => {
  let component: TodoheadComponent;
  let fixture: ComponentFixture<TodoheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoheadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
