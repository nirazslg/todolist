import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,NgModel } from '@angular/forms';
import { TodoheadComponent } from './component/todohead/todohead.component';
import { TodocontentComponent } from './component/todocontent/todocontent.component';
import { MobxAngularModule } from 'mobx-angular';
import {Routes,RouterModule} from '@angular/router';
import {routerConfig} from './todolist.router';

@NgModule({
  imports: [
    CommonModule,
    MobxAngularModule,
    FormsModule,
    RouterModule.forRoot(routerConfig)
  ],
  declarations: [TodoheadComponent, TodocontentComponent],
  exports: [TodoheadComponent,TodocontentComponent]

})
export class TodolistModule { }
