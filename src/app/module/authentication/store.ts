import { observable, computed, action } from 'mobx-angular';

class User {
  @observable username;
  @observable userpassword;
}
export default new User();
