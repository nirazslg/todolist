import { Component, OnInit } from '@angular/core';
import user from '../store';
import {Routes,Router,RouterModule} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = user;
  constructor(public router:Router) { }
  ngOnInit() {
  }
  onLogin()
  {
     console.log(this.router);
     this.router.navigate(['/todolist'], { skipLocationChange: false });
  }

}
