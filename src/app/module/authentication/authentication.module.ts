import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import {Routes,RouterModule} from '@angular/router';
import {routerConfig} from './authentication.router';
import {FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routerConfig)
  ],
  declarations: [LoginComponent]
})
export class AuthenticationModule { }
